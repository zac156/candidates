from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from api.models import Candidate
from api.serializers import CandidateSerializer
from django.db.models import Q
import requests
import random


class CandidateList(APIView):
    """
    List or filter candidates
    Post creates candidates randomly
    """

    def get(self, request, format=None):
        # Returns all candidates when no search query term is provided
        candidates = Candidate.objects.all()
        # Activates simple filter on queryset if a search query term is provided
        if request.GET.get('search_query'):
            candidates = Candidate.objects.filter(
                Q(first_name__istartswith=request.GET.get('search_query')) |
                Q(last_name__istartswith=request.GET.get('search_query')) |
                Q(city__istartswith=request.GET.get('search_query'))
            )
        serializer = CandidateSerializer(candidates, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        # Creates a random candidate from randomuser.me
        data = requests.get('https://randomuser.me/api/')
        candidate_data = data.json()['results'][0]
        name = candidate_data.get('name')

        candidate = {'title': name['title'].title(),
                     'first_name': name['first'].title(),
                     'last_name': name['last'].title(),
                     'picture_large': candidate_data['picture']['large'],
                     'picture_medium': candidate_data['picture']['medium'],
                     'picture_thumbnail': candidate_data['picture']['thumbnail'],
                     'dob': candidate_data['dob'],
                     'city': random.choice(['Cape Town', 'George', 'Johannesburg', 'Durban', 'Pretoria']),
                     'gender': candidate_data['gender'],
                     'phone': candidate_data['phone'],
                     'stars': random.randint(1, 5),
                     'address': candidate_data['location']['street'].title(),
                     'state': candidate_data['location']['state'].title(),
                     'email': candidate_data['email'],
                     'username': candidate_data['login']['username']
                     }
        serializer = CandidateSerializer(data=candidate)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
