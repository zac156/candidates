# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-28 05:19
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Candidate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('title', models.CharField(blank=True, default='', max_length=100)),
                ('first_name', models.CharField(blank=True, default='', max_length=100)),
                ('last_name', models.CharField(blank=True, default='', max_length=100)),
                ('picture_large', models.CharField(blank=True, default='', max_length=100)),
                ('picture_medium', models.CharField(blank=True, default='', max_length=100)),
                ('picture_thumbnail', models.CharField(blank=True, default='', max_length=100)),
                ('dob', models.CharField(blank=True, default='', max_length=100)),
                ('address', models.CharField(blank=True, default='', max_length=100)),
                ('state', models.CharField(blank=True, default='', max_length=100)),
                ('email', models.CharField(blank=True, default='', max_length=100)),
                ('city', models.CharField(blank=True, default='', max_length=100)),
                ('gender', models.CharField(blank=True, default='', max_length=100)),
                ('username', models.CharField(blank=True, default='', max_length=100)),
                ('phone', models.CharField(blank=True, default='', max_length=100)),
                ('stars', models.IntegerField(blank=True, default=0)),
            ],
            options={
                'ordering': ('created',),
            },
        ),
    ]
