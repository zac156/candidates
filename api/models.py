from __future__ import unicode_literals
from django.db import models


class Candidate(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=100, blank=True, default='')
    first_name = models.CharField(max_length=100, blank=True, default='')
    last_name = models.CharField(max_length=100, blank=True, default='')
    picture_large = models.CharField(max_length=100, blank=True, default='')
    picture_medium = models.CharField(max_length=100, blank=True, default='')
    picture_thumbnail = models.CharField(max_length=100, blank=True, default='')
    dob = models.CharField(max_length=100, blank=True, default='')
    address = models.CharField(max_length=100, blank=True, default='')
    state = models.CharField(max_length=100, blank=True, default='')
    email = models.CharField(max_length=100, blank=True, default='')
    city = models.CharField(max_length=100, blank=True, default='')
    gender = models.CharField(max_length=100, blank=True, default='')
    username = models.CharField(max_length=100, blank=True, default='')
    phone = models.CharField(max_length=100, blank=True, default='')
    stars = models.IntegerField(blank=True, default=0)

    class Meta:
        ordering = ('created',)
