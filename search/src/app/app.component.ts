import { Component, OnInit } from "@angular/core";
import { ApiService } from "./search.service";
import { ICandidates } from "./icandidate";
import { FormControl } from "@angular/forms";
import { Observable } from "rxjs/Observable";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
  providers: [ApiService]
})
export class AppComponent implements OnInit {
  candidateResults: Observable < ICandidates[] > ;
  candidateSearchField: FormControl;
  filteredCities: any;

  constructor(private apiSerivce: ApiService) {}

  ngOnInit(): void {
    this.candidateSearchField = new FormControl();
    
    // Initially fire search with empty string to get all candidates
    this.candidateResults = this.candidateSearchField.valueChanges
      .startWith('')
      .debounceTime(100)
      .distinctUntilChanged()
      .switchMap(term => this.apiSerivce.searchCandidate(term));

    this.filteredCities = this.candidateSearchField.valueChanges
      .startWith(null)
      .map(name => this.filterCities(name));
  }

  // Simple function which returns an array for UI star ratings loop
    range(num) {
    return new Array(num);
  }

  // Predefined city search
    cities = ["Cape Town", "George", "Johannesburg", "Durban", "Pretoria"];

  filterCities(val: string) {
    return val ?
      this.cities.filter(
        s => s.toLowerCase().indexOf(val.toLowerCase()) === 0
      ) :
      this.cities;
  }
}