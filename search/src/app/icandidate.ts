export interface ICandidates {
  id: number,
  created: string,
  title: string,
  first_name: string,
  last_name: string,
  picture_large: string,
  picture_medium: string,
  picture_thumbnail: string,
  dob: string,
  address: string,
  state: string,
  city: string,
  gender: string,
  phone: string,
  stars: string,
  email: string,
  username: string
}
