import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/Rx";
import { ICandidates } from "./icandidate";

@Injectable()
export class ApiService {
  constructor(private http: Http) {}

  private handleError(error: Response) {
    return Observable.throw(error.statusText);
  }

  // Empty search term will result in all candidates
  searchCandidate(term: string) {
    let apiURL = `/candidates/?search_query=${term}`;
    return this.http
      .get(apiURL)
      .map((response: Response) => {
        return <ICandidates[]>response.json();
      })
      .catch(this.handleError);
  }
}