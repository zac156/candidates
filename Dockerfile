FROM python:2.7-alpine

ADD . /app
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt

EXPOSE 8000