Installation
------------

To setup a local development environment::

    virtualenv env 
    source env/bin/activate

    pip install -r requirements.txt

    ./manage.py migrate
    ./manage.py runserver

Post to `/candidates/` to create a new random candidate.
